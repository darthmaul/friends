# docker stack rm rabbit
# docker stack rm worker

import os
import winsound
from threading import Thread
import multiprocessing
import subprocess
import threading
import cv2
import numpy as np
import winsound

box_size_g = 50
screen_width = 512
screen_height = 50*5
r = threading.RLock()


class RunCmdCommandThread(Thread):
    def __init__(self, cmdText):
        Thread.__init__(self)
ddd
        self.cmdText = cmdText

    def run(self):
        # this will output directly to console idk how to make it 
        # write to a variable 
        # os.system(self.cmdText)
        out = subprocess.Popen(self.cmdText, shell=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
        stdout, _ = out.communicate()
        print(stdout)
        draw_text(stdout)


class MakeSoundThread(Thread):
    def __init__(self, beepCount):
        Thread.__init__(self)
        self.beepCount = beepCount

    def run(self):
        for i in range(self.beepCount):
            winsound.Beep(2500, 50)


class Box:

    box_size = box_size_g
    main_id = 1

    def __init__(self, name, img, commnd):
        self.name = name
        self.img = img
        self.box_id = Box.main_id
        Box.main_id += 1 
        self.cmdToRun = commnd

    def draw(self):
        box_size = Box.box_size
        cv2.rectangle(self.img, (5, box_size*self.box_id),
                      (50, box_size * (self.box_id+1)), (255, 0, 0), 2)
        cv2.putText(self.img, self.name,
                    (box_size, int(box_size/2) + box_size * self.box_id),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

    def action(self):
        MakeSoundThread(self.box_id).start()
        RunCmdCommandThread(self.cmdToRun).start()
        return f"running: {self.box_id}"

    def isInside(self, x, y):
        if x < 50 and Box.box_size * (self.box_id) < y < Box.box_size * (self.box_id+1):
            return True
        return False


def draw_text(text):
    """sadasdad"""
    with r:
        cv2.rectangle(IMG, (0, box_size_g*0),
                      (screen_width, box_size_g * 1), (0, 0, 0), cv2.FILLED)
        cv2.putText(IMG, str(text), (box_size_g, int(box_size_g/2)),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)


def draw_circle(event, xxxxx, yyyyy, flags, params):
    """sadasdad"""
    if event == cv2.EVENT_LBUTTONDBLCLK:
        print(xxxxx, yyyyy)
        for bo in BOXES:
            if bo.isInside(xxxxx, yyyyy):
                print("found box ", bo.box_id)
                res = bo.action()
                draw_text(res)
                return
        print("no action clicked")


IMG = np.zeros((screen_height, screen_width, 3), np.uint8)
cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)

BOXES = []
BOXES.append(Box("list directories", IMG, "dir"))
BOXES.append(Box("remov eworker stack", IMG, [
    'docker', 'stack', 'rm', "worker"]))
BOXES.append(Box("run rabbit stack", IMG, [
    'docker', 'stack', 'deploy', '--compose-file',
    'C:\\projects\\RapidCore\\Project-Artifacts\\Docker Config\\webhooks-rabbitmq-stack.yaml',
    'rabbit']))
BOXES.append(Box("run worker stack", IMG, [
    'docker', 'stack', 'deploy', '--compose-file',
    'C:\\projects\\RapidCore\\Project-Artifacts\\Docker Config\\rapid-webhook-stack.yaml',
    'worker']))


for bo in BOXES:
    bo.draw()


while 1:
    cv2.imshow('image', IMG)
    if cv2.waitKey(20) & 0xFF == 27:
        break
cv2.destroyAllWindows